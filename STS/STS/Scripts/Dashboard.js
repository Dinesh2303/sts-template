﻿(function ($) {
    //debugger;
    var absurl = $.fn.AbsolutePath();
    var getUrl = absurl + '/Dashboard.aspx';
    var AdmissionID = StuAdmissionID; //$('#hdnMobileNo').val();
    $('#tblPaymentDetails').hide();
    $('#tblReceiptRecord').hide();
    FillDashBoadData();
    function FillDashBoadData() {
        $.getJSON(this.getUrl, { Qreqtype: 'DASHBOARD', QAdmissionID: AdmissionID }, function (result) {
            //debugger;
            $('#CurrentOpending').html(result.TotalOpenings + " Openings");
            $('#Spn_TotalRewardPoints').html(result.TotalRewardPoints + " Points Earned");
            $('#spn_TotalPendingAmt').html(result.TotalPendingAmt + " Balance Amount");
            $('#Spn_TotalReferedFriend').html(result.TotalReferedFriend + " Friends Refered");
        });
    }

    $("#grid_Student_info").on("click", "tr", function () {
        FillStudentDetails($(this).closest("tr"));
        FillReceiptList($(this).closest("tr"));
    });
    function FillStudentDetails(row) {
        var courseId = $("td", row).eq(11).html();
        var admissionid = $("td", row).eq(12).html();
        var StudentName = $("td", row).eq(3).html()
        var CourseName = $("td", row).eq(4).html();
        $('#span_Course').html(CourseName);
        $('#span_StuDentName').html(StudentName);
        $('#hdnfld_AdmissionId').val(admissionid);
        $('#hdnfld_CourseID').val(courseId);
        $('#tblPaymentDetails').show();
        $.getJSON(getUrl, { Qreqtype: 'InstallmentInfo', Qadmissionid: admissionid, QCourseID: courseId },
            function (data) {
                drawTable(data);
            })
        }

    function drawTable(data) {
        $("#tblPaymentDetails tbody").empty();
        $("#tblPaymentDetails").append("<tbody>");
        for (var i = 0; i < data.length; i++) {
            drawRow(data[i]);
        }
        $("#tblPaymentDetails").append("</tbody>");
    }

    function drawRow(rowData) {
        var Status = "Pending";
        var row = $("<tr />")

        $("#tblPaymentDetails").append(row); //this will append tr element to table... keep its reference for a while since we will add cels into it
        row.append($("<td>" + rowData.paymentid + "</td>"));
        row.append($("<td>" + rowData.AdmissionNo + "</td>"));
        row.append($("<td>" + rowData.Fname + "</td>"));
        row.append($("<td>" + rowData.Installment + "</td>"));
        row.append($("<td>" + rowData.InstalmentDate + "</td>"));
        row.append($("<td>" + rowData.Amount + "</td>"));
        if (rowData.PayStatus == "PAID")
            Status = "Paid";
        row.append($("<td>" + Status + "</td>"));
    }

    function FillReceiptList(row) {
        var courseId = $("td", row).eq(11).html();
        var admissionid = $("td", row).eq(12).html();
        var StudentName = $("td", row).eq(3).html()
        var CourseName = $("td", row).eq(4).html();
        $('#tblReceiptRecord').show();
        $.getJSON(getUrl, { Qreqtype: 'ReceiptREC', Qadmissionid: admissionid, QCourseID: courseId },
            function (data) {
                drawRecTable(data);
            })
    }
    function drawRecTable(data) {
        $("#tblReceiptRecord tbody").empty();
        $("#tblReceiptRecord").append("<tbody>");
        for (var i = 0; i < data.length; i++) {
            drawRecRow(data[i]);
        }
        $("#tblReceiptRecord").append("</tbody>");
    }
    var recCount = 0;
    function drawRecRow(rowData) {
        var Status = "Pending";
        var row = $("<tr />")
        recCount++;
        $("#tblReceiptRecord").append(row); //this will append tr element to table... keep its reference for a while since we will add cels into it
        row.append($("<td>" + recCount + "</td>"));
        row.append($("<td>" + rowData.ReceiptNo + "</td>"));
        row.append($("<td>" + rowData.PayDate + "</td>"));
        row.append($("<td>" + rowData.Amount + "</td>"));
        row.append($("<td>" + rowData.Installment + "</td>"));      
        row.append($("<td><a href=\"javascript:PrintReceipt(" + rowData.AdmissionId + "," + rowData.paymentid + ")\" id=\"2204\" title=\"Print\" class=\"btn-sm \"><i class=\"fa fa-print fa-2x\"></i></a></td>"));
        row.append($("<td class='hidencolumn'>" + rowData.paymentid + "</td>"));
        row.append($("<td class='hidencolumn'>" + rowData.AdmissionId + "</td>"));
    }
    PrintReceipt = function (AdmissionId, PaymentId) {
        debugger;
        var absurl = $.fn.AbsolutePath()
        var openUrl ="http://www.htsindia.com/admin1//Invoice-print.aspx?AId=" + AdmissionId + "&QpayId=" + PaymentId;
        window.open(openUrl, "_blank");
    }

    var AlertMessage = function (Message) {
        $.alert({
            title: 'Confirmation!',
            content: Message,
            confirmButton: 'OK',
            confirmButtonClass: 'btn-info',
            animation: 'bottom',
            icon: 'fa fa-info',
            animation: 'rotate',
            columnClass: 'col-md-4 col-md-offset-4',
            confirm: function () {
                FillStudentGiftItems();
            },
            backgroundDismiss: false
        });
    }
    function AlertMessageBOX(Message, Title, AType) {
        //debugger;
        $.msgBox({
            title: Title,
            content: Message,
            type: AType
        });
    }

})(jQuery);