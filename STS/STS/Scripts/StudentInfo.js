﻿$(document).ready(function () {
    var sMobile = StuMobileNo; //$('#hdnMobileNo').val();
    var absurl = $.fn.AbsolutePath()
    var UrltoGet = absurl + '/StudentAjaxCall.aspx'
    $("#grid_Student_info tbody").empty();
    $.getJSON(UrltoGet, { Qreqtype: 'studentinfo', QmobileNo: sMobile }, function (mydata) {
        drawTable(mydata);
    });
    function drawTable(data) {
        $("#grid_Student_info").append("<tbody>");
        for (var i = 0; i < data.length; i++) {
            drawRow(data[i]);
        }
        $("#grid_Student_info").append("</tbody>");
    }

    function drawRow(rowData) {
        var row = $("<tr />");
        $("#grid_Student_info").append(row); //this will append tr element to table... keep its reference for a while since we will add cels into it
        //row.append($("<td>" + rowData.AdmissionId + "</td>"));
        row.append($("<td>" + rowData.AdmissionDid + "</td>"));
        row.append($("<td>" + rowData.AdmissionNo + "</td>"));
        row.append($("<td>" + rowData.AdmissionDate + "</td>"));
        row.append($("<td>" + rowData.Name + "</td>"));
        row.append($("<td>" + rowData.Course + "</td>"));
        //row.append($("<td>" + rowData.CourseId + "</td>"));
        row.append($("<td>" + rowData.Franchise + "</td>"));
        row.append($("<td>" + rowData.MobileNo + "</td>"));
        row.append($("<td>" + rowData.BatchNo + "</td>"));
        row.append($("<td>" + rowData.FeedbackNo + "</td>"));
        row.append($("<td>" + rowData.BalanceAmt + "</td>"));
        row.append($("<td><a class=\"btn btn-info btn-sm\" id=\"btn_Click\" ><i class=\"fa fa-hand-o-left \"></i>  Click</a></td>"));
        row.append($("<td class='hidencolumn'>" + rowData.CourseId + "</td>"));
        row.append($("<td class='hidencolumn'>" + rowData.AdmissionId + "</td>"));
        row.append($("<td class='hidencolumn'>" + rowData.Status + "</td>"));
        row.append($("<td class='hidencolumn'>" + rowData.TrainerID + "</td>"));
        row.append($("<td class='hidencolumn'>" + rowData.CounsellerID + "</td>"));
        row.append($("<td class='hidencolumn'>" + rowData.Fr_id + "</td>"));
        row.append($("<td class='hidencolumn'>" + rowData.FeedbackID + "</td>"));
        row.append($("<td class='hidencolumn'>" + rowData.TrainerName + "</td>"));
    }
   
});
