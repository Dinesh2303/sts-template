﻿// JScript File
(function ($) {
    //debugger;
    /*----------------------------------------------------------------------*/
    /*Admission Master class
    /*Dinesh
    /*----------------------------------------------------------------------*/
    var absurl = $.fn.AbsolutePath();
    var sPath = window.location.hostname;
    var getUrl = absurl + '/StudentAjaxCall.aspx';


    $.BindCenter = function (Fr_ID, controlId) {
        var $selectFranchise = $('#' + controlId);
        $selectFranchise.find('option').remove();
        $.getJSON(getUrl, { Qreqtype: 'F' }, function (result) {
            for (var i = 0; i < result.length; i++) {
                var frId = result[i].Fr_Id == "0" ? "" : result[i].Fr_Id;
                $selectFranchise.append('<option value=' + frId + '>' + result[i].Fr_name + '</option>');
            }
            $selectFranchise.val(Fr_ID);
            if (UserLevel != 1) {
                $selectFranchise.attr("disabled", "disabled");
            }
        });
    }
    $.BindCourseCategory = function (controlId) {
        var $selectCategory = $('#' + controlId);
        $selectCategory.find('option').remove();
        $.getJSON(getUrl, { Qreqtype: 'CourseCategory' }, function (result) {
            for (var i = 0; i < result.length; i++) {
                var Course_CateID = result[i].Course_CateID == "0" ? "" : result[i].Course_CateID;
                $selectCategory.append('<option value=' + Course_CateID + '>' + result[i].CourseCategory + '</option>');
            }
        });
    }
    $.BindCourseSubCategory = function (Course_CateID, controlId) {
        var $selectSubCategory = $('#' + controlId);
        $selectSubCategory.find('option').remove();
        $.getJSON(getUrl, { Qreqtype: 'CourseSubCategory', Cate_Id: Course_CateID }, function (result) {
            if (result.length > 1) {
                for (var i = 0; i < result.length; i++) {
                    var Course_SubCateID = result[i].Course_SubCateID == "0" ? "" : result[i].Course_SubCateID;
                    $selectSubCategory.append('<option value=' + Course_SubCateID + '>' + result[i].CourseSubCategory + '</option>');
                }
            }
            else {
                for (var i = 0; i < result.length; i++) {
                    var Course_SubCateID = result[i].Course_SubCateID == "0" ? "" : result[i].Course_SubCateID;
                    $selectSubCategory.append('<option value=' + Course_SubCateID + '>' + result[i].CourseSubCategory + '</option>');
                }
                var CourseControlId=""
                if (controlId == "cmblCourseSubCategory") {
                    CourseControlId = "CmblCourse";
                }
                else {
                    CourseControlId = "cmbrCourse";
                }
                $.BindCourseByID(CourseControlId,Course_CateID, 0);
            }

        });
    }
    $.BindCourseByID = function (controlId,Course_CateID, CourseSubCateid) {
        $('#' + controlId).find('option').remove();
        $.getJSON(getUrl, { Qreqtype: 'COURSEID', Cate_Id: Course_CateID, SubCate_Id: CourseSubCateid }, function (result) {
            for (var i = 0; i < result.length; i++) {
                var Course_id = result[i].Course_id == "0" ? "" : result[i].Course_id;
                $('#' + controlId).append('<option value=' + Course_id + '>' + result[i].Course + '</option>');
            }
        });

    }


    $.BindTrainerByID = function (fr_id, CourseID, CmbTrainer) {
        var $selectTrainer = $('#' + CmbTrainer);
        $selectTrainer.find('option').remove();
        $.getJSON(getUrl, { Qreqtype: 'TRAINER', QSelCourses: CourseID, QFr_id: fr_id   }, function (result) {
            for (var i = 0; i < result.length; i++) {
                $selectTrainer.append('<option value=' + result[i].Trainer_ID + '>' + result[i].Trainer_Name + '</option>');
            }
        });
    }
    $.BindTrainerByBatchType = function (fr_id, CourseID, CmbTrainer, BatchType, CourseCateId, CourseSubCateId) {
        
        var $selectTrainer = $('#' + CmbTrainer);
        $selectTrainer.find('option').remove();
        $.getJSON(getUrl, {
            Qreqtype: 'TRAINERBYBATCHTYPE', QSelCourses: CourseID, QFr_id: fr_id, BatchType: BatchType,
            CourseCateId: CourseCateId, CourseSubCateId: CourseSubCateId
        }, function (result) {
            for (var i = 0; i < result.length; i++) {
                $selectTrainer.append('<option value=' + result[i].Trainer_ID + '>' + result[i].Trainer_Name + '</option>');
            }
        });
    }
    //Fill Batch
    $.BindBatchByID = function (fr_id, CourseID , trainerId, cmbid) {
        //debugger;
        var $selectCMbBatch = $('#' + cmbid);
        $selectCMbBatch.find('option').remove();
        $.getJSON(getUrl, { Qreqtype: 'BATCH', QSelCourses: CourseID, QtrainerId: trainerId, QFr_id: fr_id }, function (result) {
            for (var i = 0; i < result.length; i++) {
                $selectCMbBatch.append('<option value=' + result[i].BatchId + '>' + result[i].BatchCode + '</option>');
            }
        });
    }
    $.BindBatchList = function (fr_id, CourseID, CourseCateId, CourseSubCateId, cmbid) {
        //debugger;
        var $selectCMbBatch = $('#' + cmbid);
        $selectCMbBatch.find('option').remove();
        $.getJSON(getUrl, {
            Qreqtype: 'FILTERBATCH', QSelCourses: CourseID, QFr_id: fr_id,
            CourseCateId: CourseCateId, CourseSubCateId: CourseSubCateId
        }, function (result) {
            $selectCMbBatch.find('option').remove();
            for (var i = 0; i < result.length; i++) {
                $selectCMbBatch.append('<option value=' + result[i].BatchId + '>' + result[i].BatchCode + '</option>');
            }
        });
    }
    $.BindBatchFilter = function (fr_id, CourseID, trainerId, cmbid, CourseCateId, CourseSubCateId, batchstatus) {
        //debugger;
        var $selectCMbBatch = $('#' + cmbid);
        $selectCMbBatch.find('option').remove();
        $.getJSON(getUrl, {
            Qreqtype: 'FILTERBATCH', QSelCourses: CourseID, QtrainerId: trainerId, QFr_id: fr_id,
            CourseCateId: CourseCateId, CourseSubCateId: CourseSubCateId, batchstatus: batchstatus
        }, function (result) {
            $selectCMbBatch.find('option').remove();
            for (var i = 0; i < result.length; i++) {
                $selectCMbBatch.append('<option value=' + result[i].BatchId + '>' + result[i].BatchCode + '</option>');
            }
        });
    }
    // Fill Batch Type 
    $.BindBatchType = function (CmbBatchType) {
        //debugger;
        var $selectCMbBatch = $('#' + CmbBatchType);
        $selectCMbBatch.find('option').remove();
        $.getJSON(getUrl, { Qreqtype: 'BATCHTYPE' }, function (result) {
            for (var i = 0; i < result.length; i++) {
                if (CmbBatchType == 'CmblBatchType') {
                    $('#CmblTrainer').hide();
                    $('#CmblBatch').hide();
                    if (result[i].BatchTypelList != 'New Batch' && result[i].BatchTypelList != 'Center TransFer') {
                        $selectCMbBatch.append('<option value=' + result[i].BatchTypeId + '>' + result[i].BatchTypelList + '</option>');
                    }
                    $selectCMbBatch.val("1");
                }
                else {
                    if (result[i].BatchTypelList != 'New Batch' && result[i].BatchTypelList != 'Tempary'
                        && result[i].BatchTypelList != 'Center TransFer') {
                        $selectCMbBatch.append('<option value=' + result[i].BatchTypeId + '>' + result[i].BatchTypelList + '</option>');
                    }
                }

            }
        });
    }
    /*Model course*/
    $.model_BindCourseSubCategory = function (Course_CateID, controlId) {
        //debugger
        var $selectSubCategory = $('#' + controlId);
        $selectSubCategory.find('option').remove();

        $.getJSON(getUrl, { Qreqtype: 'CourseSubCategory', Cate_Id: Course_CateID }, function (result) {
            //debugger;
            if (result.length > 1) {
                for (var i = 0; i < result.length; i++) {
                    $selectSubCategory.append('<option value=' + result[i].Course_SubCateID + '>' + result[i].CourseSubCategory + '</option>');
                }
            }
            else {
                for (var i = 0; i < result.length; i++) {
                    $selectSubCategory.append('<option value=' + result[i].Course_SubCateID + '>' + result[i].CourseSubCategory + '</option>');
                }
                $.model_BindCourseByID( Course_CateID, 0);
            }

        });
    }
    $.model_BindCourseByID = function (Course_CateID, CourseSubCateid) {
        $('#mdl_CmbCourse').find('option').remove();
        //debugger;
        $.getJSON(getUrl, { Qreqtype: 'COURSEID', Cate_Id: Course_CateID, SubCate_Id: CourseSubCateid }, function (result) {
            //debugger;
            for (var i = 0; i < result.length; i++) {
                $('#mdl_CmbCourse').append('<option value=' + result[i].Course_id + '>' + result[i].Course + '</option>');
            }
        });
    }
    $.model_BindBatchNo = function (FrID) {
        $('#idbatchNo').val('');
        $.getJSON(getUrl, { Qreqtype: 'getBatchNo', QFrId: FrID }, function (result) {
            $('#idbatchNo').val(result.BatchNo);
        });
    }
    /*end*/
    //Get Franchise
    function FillCenterCombo() {
        // debugger;
        var $selectFranchise = $('#cmbCenter');
        $selectFranchise.find('option').remove();

        $.getJSON(this.getUrl, { Qreqtype: 'F' }, function (result) {
            for (var i = 0; i < result.length; i++) {
                //options += '<option value="' + result[i].Fr_Id + '">' + result[i].Fr_name + '</option>';
                $selectFranchise.append('<option value=' + result[i].Fr_Id + '>' + result[i].Fr_name + '</option>');
            }

            $('#cmbCenter').val(Fr_ID);
            if (UserLevel != 1) {
                $('#cmbCenter').attr("disabled", "disabled");
            }

        });
    }
    //Fill Course
    function FillCourseByID() {
        //debugger;
        var $selectCourse = $('#CmbCourse');
        $selectCourse.find('option').remove();
        $.getJSON(this.getUrl, { Qreqtype: 'COURSE' }, function (result) {
            for (var i = 0; i < result.length; i++) {
                $selectCourse.append('<option value=' + result[i].course_id + '>' + result[i].Course + '</option>');
            }

        });
    }
    //Fill User
    function FillUser() {
        //debugger;
        var $selectUser = $('#CmbUser');
        $selectUser.find('option').remove();
        $.getJSON(this.getUrl, { Qreqtype: 'USER' }, function (result) {
            for (var i = 0; i < result.length; i++) {
                $selectUser.append('<option value=' + result[i].Userid + '>' + result[i].UserName + '</option>');
            }

        });
    }




})(jQuery);