﻿$(function () {
    var absurl = $.fn.AbsolutePath()
    var UrltoGet = absurl + '/Dashboard.aspx'

    $('#btngenerateOTP').hide();
    $('.show-login').on('click', function () {
        $('.login-body').removeClass('openjoinus');
        $('#btn_login').show();
        $('#btngenerateOTP').hide();
        ClearField();
    });
    $('.show_Joinus').on('click', function () {
        $('.login-body').addClass('openjoinus');        
        $('#btn_login').hide();        
        $('#btngenerateOTP').show();
        ClearField();
    });
    $('.show-forgot').on('click', function () {
        $('.login-body').addClass('openjoinus');   
        $('#btn_login').hide(); 
        $('#btngenerateOTP').show();
        ClearField();
    });

    $('#btngenerateOTP').on('click', function () {
        //debugger;
        var txtMobileNo = $.trim($("[id$=txtregMobileNo]").val());
        if (txtMobileNo == "") {
            $("[id$=txtregMobileNo]").focus();
            $.fn.errorMsg("Opps!! Mobile  Can't be blank.");
            return false;
        }
        if (isNaN(txtMobileNo) || txtMobileNo.indexOf(" ") != -1) {
            $("[id$=txtregMobileNo]").focus();
            $.fn.errorMsg("Opps!! Enter numeric value.");
            return false;
        }
        if (txtMobileNo.length > 10) {
            $("[id$=txtregMobileNo]").focus();
            $.fn.errorMsg("Opps!! Enter 10 digit mobile No");
            return false;
        }
        if (txtMobileNo != "") {
            var dataToSend = {
                sMobileNo: txtMobileNo, MethodName: 'generateOTP'
            };

            $.ajax(
                {
                    url: 'StudentAjaxCall.aspx',
                    data: dataToSend,
                    async: true,
                    dataType: 'text',
                    type: 'POST',
                    success: function (data) {
                        
                        if (data == "200") {
                            $('#btn_login').show();
                            $('#btngenerateOTP').hide();
                            $.fn.successMsg("Your UserId and Password has been sent to your mobile no" + txtMobileNo);                         
                            return false;
                        }
                        if (data == "201") {
                            $("[id$=txtregMobileNo]").focus();
                            $.fn.errorMsg("Opps!! Mobile no provided by you is invelid.");
                            return false;
                        }
                        if (data == "202") {
                            //$("[id$=txtregMobileNo]").focus();
                            AlertMessage("Opps!! You seem not to be the part of HTS.","Validate","error");
                            return false;
                        }
                        ClearField();
                    },
                    error: function () {
                        $("[id$=txtregMobileNo]").focus();
                        $.fn.errorMsg("Opps!! Failure to load file");
                        return false;
                    }
                });
        }

    });
    $('#btn_login').on('click', function () {
        var txtUserName = $.trim($("[id$=txtUserName]").val());
        var txtPassword = $.trim($("[id$=txtPassword]").val());
       
        if (txtUserName == "") {
            $("[id$=txtUserName]").focus();
            $.fn.errorMsg("Opps!! User Name can't be blank.");
            return false;
        }
        if (txtPassword == "") {
            $("[id$=txtPassword]").focus();
            $.fn.errorMsg("Opps!! Password can't be blank.");
            return false;
        }

        var dataToSend = {
            QsUsername: txtUserName, QsPassword: txtPassword, MethodName: 'btnLoginCall'
        };
        $.ajax(
            {
                url: 'StudentAjaxCall.aspx',
                data: dataToSend,
                async: true,
                dataType: 'text',
                type: 'POST',
                success: function (data) {
                    ClearField();
                    if (data == "202") {
                        $.fn.errorMsg("Opps!! You seem not to be the part of HTS.");
                        $("[id$=txtUserName]").focus();
                        return false;
                    }
                    if (data == "200") {
                        var url = UrltoGet;
                        $(location).attr('href', url);
                        return true;
                    }
                    if (data == "201") {
                        $.fn.errorMsg("Opps!! There is some error on server.");
                        $("[id$=txtUserName]").focus();
                        return false;
                    }
                },
                error: function () {
                    $.fn.errorMsg("Opps!! Failure to load file");
                    return false;
                }

            });
        return false;
    });
    function AlertMessage(Message, Title, AType) {
        //debugger;
        $.msgBox({
            title: Title,
            content: Message,
            type: AType
        });

    }
    function ClearField()
    {
        $('#txtUserName').val('');
        $('#txtPassword').val('');
        $('#txtregMobileNo').val('');        
    }

});                                       //end of main jQuery Ready method