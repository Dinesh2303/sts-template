﻿$(function () {
    var absurl = $.fn.AbsolutePath()
    var UrltoGet = absurl + '/ChangePassword.aspx'

    $('#txtMobileNo').val(StuMobileNo);
    $('#txtUsername').val(sUserName);

    $('#aspnetForm')
        .formValidation({
            framework: 'bootstrap',
            button: {
                selector: '#btnChangePws',
                disabled: 'disabled'
            },
            //icon: {
            //    valid: 'glyphicon glyphicon-ok',
            //    invalid: 'glyphicon glyphicon-remove',
            //    validating: 'glyphicon glyphicon-refresh'
            //},
            fields: {
                mobileno: {
                    validators: {
                        notEmpty: {
                            message: 'Mobile Number is required'
                        }

                    }
                },
                UserName: {
                    validators: {
                        notEmpty: {
                            message: 'User Name is required'
                        }

                    }
                },
                NewPassword: {
                    validators: {
                        notEmpty: {
                            message: 'New Password is required'
                        }

                    }
                },
                confirmPws: {
                    validators: {
                        notEmpty: {
                            message: 'Confirm New Password is required'
                        },
                        identical: {
                            field: 'NewPassword',
                            message: 'The password and its confirm are not the same'
                        }
                    }
                }

            }
        }).on('success.form.fv', function (e) {

            //alert("sucess");
            var mobileNo = $('#txtMobileNo').val();
            var oldPws = ""; //$('#hdnfld_CourseID').val();
            var newPassword = $('#txtNewPassword').val();

            var dataToSend = {
                Qreqtype: 'SubmitData', QMobileNo: mobileNo, QNewPassword: newPassword, QoldPws: oldPws
            };
            //debugger;
            //$.blockUI({ message: $('#domMessage') });
            $.ajax({
                type: "POST",
                url: UrltoGet,
                data: dataToSend,
                //contentType: "application/json; charset=utf-8",
                dataType: "json",
                cache: false,
                success: function (data) {
                    //alert(data);                   
                    //debugger;
                    var msg = "";
                    if (data == "1") {
                        msg = "Successfully changed your password.";
                    }
                    else if (data == "0") {
                        msg = "OOPS! User does not exist.";
                    }
                    else
                        msg = "OOPS! There is some technical problem on Server.";
                    AlertMessage(msg, "Confirmation", "success");
                    return false;
                },
                failure: function () {
                    var msg = "There is some problem on server.";
                    $.HtsAlertMsg("Alert", msg, "alert", "");
                    return false;

                },
                complete: function (xhr, status) {
                    //$("body").unblock();
                    ClearField();
                }                       //End of Ajax
            });
            return false;
        });
    function ClearField() {
        $('#txtNewPassword').val('');
        $('#txtConfirmPassword').val('');
    }
    function AlertMessage(Message, Title, AType) {
        //debugger;
        $.msgBox({
            title: Title,
            content: Message,
            type: AType
        });

    }
});                                       //end of main jQuery Ready method