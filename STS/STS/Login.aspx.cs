﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace STS
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["logout"] != null)
            {
                //SessionWrapper.StudentMobileNo = "";
                Session.Abandon();
            }
        }
        protected void btn_login_Click(object sender, EventArgs e)
        {
            Response.Redirect("AuthOtp.aspx");
        }
        protected void btngenerateOTP_Click(object sender, EventArgs e)
        {

        }
    }
}