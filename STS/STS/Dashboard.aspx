﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="STS.Dashboard" %>

<asp:Content ID="DashboardContent" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
                <h2>Dashboard</h2>
                <h5>Welcome to STS
                                   , Love to see you back. </h5>

            </div>
        </div>
        <!-- /. ROW  -->
        <hr>

        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="panel panel-back noti-box">
                    <span class="icon-box bg-color-red set-icon">
                        <i class="fa fa-envelope-o"></i>
                    </span>
                    <div class="text-box">
                        <p class="main-text" id="CurrentOpending">0 Openings</p>
                        <p class="text-muted">Openings</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="panel panel-back noti-box">
                    <span class="icon-box bg-color-green set-icon">
                        <i class="fa fa-shopping-cart"></i>
                    </span>
                    <div class="text-box">
                        <p class="main-text" id="Spn_TotalRewardPoints">0 Points Earned</p>
                        <p class="text-muted">Points</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="panel panel-back noti-box">
                    <span class="icon-box bg-color-blue set-icon">
                        <i class="fa fa-rupee"></i>
                    </span>
                    <div class="text-box">
                        <p class="main-text" id="spn_TotalPendingAmt">0 Balance Amount</p>
                        <p class="text-muted">Pending</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="panel panel-back noti-box">
                    <span class="icon-box bg-color-brown set-icon">
                        <i class="fa fa-share-alt"></i>
                    </span>
                    <div class="text-box">
                        <p class="main-text" id="Spn_TotalReferedFriend">0 Friends Refered</p>
                        <p class="text-muted">Friends</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="panel panel-back noti-box">
                    <span class="icon-box bg-color-red set-icon">
                        <i class="fa fa-envelope-o"></i>
                    </span>
                    <div class="text-box">
                        <p class="main-text" id="CurrentOpending">0 Openings</p>
                        <p class="text-muted">Openings</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="panel panel-back noti-box">
                    <span class="icon-box bg-color-green set-icon">
                        <i class="fa fa-shopping-cart"></i>
                    </span>
                    <div class="text-box">
                        <p class="main-text" id="Spn_TotalRewardPoints">0 Points Earned</p>
                        <p class="text-muted">Points</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="panel panel-back noti-box">
                    <span class="icon-box bg-color-blue set-icon">
                        <i class="fa fa-rupee"></i>
                    </span>
                    <div class="text-box">
                        <p class="main-text" id="spn_TotalPendingAmt">0 Balance Amount</p>
                        <p class="text-muted">Pending</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="panel panel-back noti-box">
                    <span class="icon-box bg-color-brown set-icon">
                        <i class="fa fa-share-alt"></i>
                    </span>
                    <div class="text-box">
                        <p class="main-text" id="Spn_TotalReferedFriend">0 Friends Refered</p>
                        <p class="text-muted">Friends</p>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" id="hdnfld_CourseID" value="3">
        <input type="hidden" id="hdnfld_AdmissionId" value="1374">
    </div>
</asp:Content>
