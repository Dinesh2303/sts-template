﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="STS.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="Shortcut Icon" href="favicon.ico" />
    <link rel="canonical" href="#" />
    <title>STS</title>
    <%--<link href="js/jquery-ui.min.css" rel="stylesheet" />--%>
    <%--  <link href="../Content/css/JQueryThems/jquery-ui.min.css" rel="stylesheet" />--%>
    <link href="login-css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="login-css/main.css" rel="stylesheet" type="text/css" />
    <%--<link href="assets/css/font-awesome.css" rel="stylesheet" />--%>
    <link href="assets/font-awesome/font-awesome.css" rel="stylesheet" />
    <link href="login-css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="login-css/Login.css" rel="stylesheet" type="text/css" />
    <%--    <script src="assets/js/jquery-1.10.2.js"></script>--%>
    <%--    <script src="js/jquery-ui/jquery-ui/jquery-ui.min.js"></script>--%>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <%--    <link href="../Content/css/msgBoxLight.css" rel="stylesheet" />--%>
    <%-- <script src="../Scripts/jquery.msgBox.js"></script>
    <script src="../Scripts/HtsHalper.js"></script>--%>
    <style type="text/css">
        .form-row {
            margin-bottom: 0px !important;
        }

        .login-body input.light {
            height: 45px !important;
        }
    </style>

</head>
<body class="login">
    <form id="defaultform" runat="server">
        <%--        <link href="Css/bootstrapValidator.css" rel="stylesheet" />--%>
        <%-- <script src="js/bootstrapValidator.js"></script>--%>

        <%--      <script src="Scripts/StudentLogin.js"></script>--%>
        <header>
            <div class="logo" style="height: 106px;">
                <a href="#" title="Home">
                    <img alt="tech-mahindra" src="Images/logo.jpg" /></a>
            </div>
        </header>
        <div class="login-area-wrap">
            <div class="c-left">
            </div>
            <div class="c-right">
                <div class="login-frame">
                    <div class="login-header">
                        <h2 class="prox-heavy login-title">SIGN IN</h2>
                    </div>
                    <div class="login-body">
                        <div class="login-default-wrap">
                            <div>
                                <div class="row-fluid">
                                    <div class="form-row">

                                        <input name="txtUserName" type="text" id="txtUserName" class="light fullwidth"
                                            placeholder="Enter your username" />
                                        <span class="adds-on"><i class="fa fa-user"></i></span>
                                    </div>
                                    <div class="form-row">

                                        <input name="txtPassword" type="password" id="txtPassword"
                                            class="light fullwidth"
                                            placeholder="Enter your password" />
                                        <span class="adds-on"><i class="fa fa-lock"></i></span>
                                    </div>
                                    <div class="keep-sign">
                                        <label class="checkbox">
                                            <input type="checkbox">
                                            Keep me signed in
                                        </label>
                                    </div>
                                    <div class="forgot">
                                        <a class="show-forgot" href="#">Forgot Password?</a>
                                    </div>
                                    <div class="joinus">
                                        New User ?
									<a href="#toregister" class="show_Joinus">Join us</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="login-joinus-wrap">
                            <div>
                                <div class="row-fluid">
                                    <div class="form-row">

                                        <input name="txtregMobileNo" type="text" maxlength="10" id="txtregMobileNo"
                                            class="light fullwidth" placeholder="Enter your mobile No" />
                                        <span class="adds-on"><i class="fa fa-mobile"></i></span>
                                    </div>
                                    <div class="joinus">
                                        Already have userid & password ?
									<a href="#tologin" class="show-login">log in </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="login-footer" align="right">
                        <asp:Button ID="btn_login" CssClass="login-btn" runat="server" Text="Login" OnClick="btn_login_Click" />
                        <input class="login-btn" id="btn_send" value="SEND PASSWORD" name="btn_send" type="submit"
                            style="width: 100%; cursor: pointer; display: none;" />
                        <input class="login-btn" id="btngenerateOTP" value="Generate your password" name="btngenerateOTP" type="submit"
                            style="width: 100%; cursor: pointer; display: none;" />
                    </div>
                </div>
            </div>
            <div class="msg-holder">
                <div class="msg-container">
                    <div class="msg-body success">
                        <div class="notif-icon">
                            <i class="fa fa-check"></i>
                        </div>
                        <div class="msg-body-text">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer style="text-align: center">
            <div class="copy-text">
                Copyright © 2019 STS. All Rights Reserved. Website Developed
                    by <a href="#" target="_blank">Company Name</a>
            </div>

            <ul class="contact_address">
                <li><i class="fa fa-envelope"></i><a href="#">abc@test.com</a></li>
            </ul>

            <ul class="footer_social_links">
                <li><a href="#" target="_blank" title="facebook"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#" target="_blank" class="fa fa-twitter" title="Twitter"><i class="twitter"></i></a></li>
                <li><a href="#" target="_blank" title="LinkedIn"><i class="fa fa-linkedin"></i></a></li>
                <li><a href="#" target="_blank" class="pinterest" title="YouTube"><i class="fa fa-youtube"></i></a></li>
            </ul>
        </footer>
    </form>

</body>
</html>

