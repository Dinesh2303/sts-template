﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserCourseList.ascx.cs" Inherits="HTS.WEB.Students.UserControl.UserCourseList" %>

<script src="Scripts/StudentInfo.js"></script>
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover" id="grid_Student_info">
        <thead>
            <tr>
                <%--  <th>AdmissionId</th>--%>
                <th>ID </th>
                <th>Adm. No</th>
                <th>Adm. Date</th>
                <th>Name</th>
                <th>Course</th>
                <%-- <th>CourseId</th>--%>
                <th>Center</th>
                <th>Mobile No</th>
                <th>Batch No</th>
                <th>FeedBack No</th>
                <th>Balance Amt.</th>
                <th>Action</th>
                <th class="hidencolumn">CourseId</th>
                <th class="hidencolumn">AdmissionId</th>
                <th class="hidencolumn">Status</th>
                <th class="hidencolumn">TrainerID</th>
                <th class="hidencolumn">CounsellerID</th>
                <th class="hidencolumn">Fr_id</th>
                <th class="hidencolumn">FeedbackId</th>
                <th class="hidencolumn">TrainerName</th>
            </tr>
        </thead>
    </table>
</div>
