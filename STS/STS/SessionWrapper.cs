﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace STS
{
    public class SessionWrapper
    {
        public static string UserName
        {
            get { return GetFromSession<string>("UserName"); }
            set { SetInSession<string>("UserName", value); }
        }
        public static string UserRole
        {
            get { return GetFromSession<string>("UserRole"); }
            set { SetInSession<string>("UserRole", value); }
        }
        public static Int32 UserLevel
        {
            get { return GetFromSession<Int32>("UserLevel"); }
            set { SetInSession<Int32>("UserLevel", value); }
        }      
        public static Int32 loginId
        {
            get { return GetFromSession<Int32>("UID"); }
            set { SetInSession<Int32>("UID", value); }
        }
        public static string Message
        {
            get { return GetFromSession<string>("Message"); }
            set { SetInSession<string>("Message", value); }
        }
        public static string DisplayName
        {
            get { return GetFromSession<string>("DisplayName"); }
            set { SetInSession<string>("DisplayName", value); }
        }
        private static T GetFromSession<T>(string key)
        {
            object obj = HttpContext.Current.Session[key];
            if (obj == null)
            {
                return default(T);
            }
            return (T)obj;
        }
        private static void SetInSession<T>(string key, T value)
        {
            if (value == null)
            {
                HttpContext.Current.Session.Remove(key);
            }
            else
            {
                HttpContext.Current.Session[key] = value;
            }
        }
    }
}