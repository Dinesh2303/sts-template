﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AuthOtp.aspx.cs" Inherits="STS.AuthOtp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="Shortcut Icon" href="favicon.ico" />
    <link rel="canonical" href="#" />
    <title>STS</title>
    <%--<link href="js/jquery-ui.min.css" rel="stylesheet" />--%>
    <%--  <link href="../Content/css/JQueryThems/jquery-ui.min.css" rel="stylesheet" />--%>
    <link href="login-css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="login-css/main.css" rel="stylesheet" type="text/css" />
    <%--<link href="assets/css/font-awesome.css" rel="stylesheet" />--%>
    <link href="assets/font-awesome/font-awesome.css" rel="stylesheet" />
    <link href="login-css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="login-css/Login.css" rel="stylesheet" type="text/css" />
    <%--    <script src="assets/js/jquery-1.10.2.js"></script>--%>
    <%--    <script src="js/jquery-ui/jquery-ui/jquery-ui.min.js"></script>--%>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <%--    <link href="../Content/css/msgBoxLight.css" rel="stylesheet" />--%>
    <%-- <script src="../Scripts/jquery.msgBox.js"></script>
    <script src="../Scripts/HtsHalper.js"></script>--%>
    <style type="text/css">
        .form-row {
            margin-bottom: 0px !important;
        }

        .login-body input.light {
            height: 45px !important;
        }
    </style>
    <link href="Css/pomodoro-clock.css" rel="stylesheet" />
    <link href='https://fonts.googleapis.com/css?family=Dosis:300,400,600,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body class="login">
    <form id="defaultform" runat="server">
        <%--        <link href="Css/bootstrapValidator.css" rel="stylesheet" />--%>
        <%-- <script src="js/bootstrapValidator.js"></script>--%>

        <%--      <script src="Scripts/StudentLogin.js"></script>--%>
        <header>
            <div class="logo" style="height: 106px;">
                <a href="#" title="Home">
                    <img alt="tech-mahindra" src="Images/logo.jpg" /></a>
            </div>

        </header>

        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="space">
                    <div class="container">
                        <div id="circleClock" class="circle-clock">
                            <div id="clockTime" class="clock-time">
                                <h3>
                                    <span id="minutes">2</span>:
                        <span id="seconds">00</span>
                                </h3>
                            </div>
                            <%-- <div id="clockStatus" class="status">Click to start!</div>--%>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="login-area-wrap">
                    <div class="c-left">
                    </div>
                    <div class="c-right">
                        <div class="login-frame">
                            <div class="">
                                <h2 class="prox-heavy login-title">Verify OTP</h2>
                            </div>
                            <div class="login-body">
                                <div class="login-default-wrap">
                                    <div>
                                        <div class="row-fluid">
                                            <div class="form-row">
                                                <input name="txtUserName" type="text" id="txtUserName" class="light fullwidth"
                                                    placeholder="Enter your OTP" />
                                                <span class="adds-on"><i class="fa fa-key"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="login-joinus-wrap">
                                    <div>
                                        <div class="row-fluid">
                                            <div class="form-row">

                                                <input name="txtregMobileNo" type="text" maxlength="10" id="txtregMobileNo"
                                                    class="light fullwidth" placeholder="Enter your mobile No" />
                                                <span class="adds-on"><i class="fa fa-mobile"></i></span>
                                            </div>
                                            <div class="joinus">
                                                Already have userid & password ?
									<a href="#tologin" class="show-login">log in </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="login-footer" align="right">
                                <asp:Button ID="btn_login" CssClass="login-btn" runat="server" Text="Verify OTP" OnClick="btn_login_Click" />                                              
                            </div>
                        </div>
                    </div>
                    <div class="msg-holder">
                        <div class="msg-container">
                            <div class="msg-body success">
                                <div class="notif-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="msg-body-text">
                                    dfsdfgs
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6">
            </div>
        </div>

        <footer style="text-align: center">
            <div class="copy-text">
                Copyright © 2019 STS. All Rights Reserved. Website Developed
                    by <a href="#" target="_blank">Company Name</a>
            </div>

            <ul class="contact_address">
                <li><i class="fa fa-envelope"></i><a href="#">abc@test.com</a></li>
            </ul>

            <ul class="footer_social_links">
                <li><a href="#" target="_blank" title="facebook"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#" target="_blank" class="fa fa-twitter" title="Twitter"><i class="twitter"></i></a></li>
                <li><a href="#" target="_blank" title="LinkedIn"><i class="fa fa-linkedin"></i></a></li>
                <li><a href="#" target="_blank" class="pinterest" title="YouTube"><i class="fa fa-youtube"></i></a></li>
            </ul>
        </footer>
    </form>
    <script src="js/pomodoro-clock.js"></script>
    <script>
        $(document).ready(function () {
            debugger;
            if (timerOn) {
                timerOn = false;
                $('#clockStatus').text('Click to start again!');
            } else {
                timerOn = true;
                timerLoop();
                $('#clockStatus').text('Timer is running...');
            }
        });
    </script>
</body>
</html>

